# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
LighteningTalkMaps::Application.config.secret_key_base = '1b5bce8c0f4ba750b19fa9a5c2f8f205127cd686f9e2b3d3369c48ecbe1faac4da3e7e994dbe2c77eb6a4ea03cb8093ddaf18ef72bf4a3acf21bf2daf89bed47'
